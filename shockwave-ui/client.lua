local INPUT_GROUP_DEFAULT = 0
local INPUT_ENTER_CHEAT_CODE = 243

local lockedAt = 0
local isVisible = false
local isControlLocked = false

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    local isControlPressed = IsControlPressed(
      INPUT_GROUP_DEFAULT,
      INPUT_ENTER_CHEAT_CODE
    )

    if isControlPressed then
      if not isControlLocked then
        if not isVisible then
          isVisible = true

          TransitionToBlurred(100.5)
          PlaySoundFrontend(-1, "FocusIn", "HintCamSounds", 1)
          StartScreenEffect('MenuMGSelectionTint')
          StopScreenEffect('SwitchHUDOut')
          StartScreenEffect('SwitchHUDIn')
          SendNUIMessage({ command = 'show-ui' })
        else
          isVisible = false

          TransitionFromBlurred(100.5)
          StopScreenEffect('MenuMGSelectionTint')
          StopScreenEffect('SwitchHUDIn')
          StartScreenEffect('SwitchHUDOut')
          SendNUIMessage({ command = 'hide-ui' })
        end

        isControlLocked = true
        lockedAt = GetGameTimer()
      end
    elseif isControlLocked then
      isControlLocked = GetTimeDifference(GetGameTimer(), lockedAt) <= 250
    end

    if isVisible then
      HideHudAndRadarThisFrame()
    end
  end
end)
