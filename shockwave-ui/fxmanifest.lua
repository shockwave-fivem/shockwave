fx_version 'cerulean'
game 'gta5'

files {
  'index.html',
  'ui.css',
  'ui.js'
}

ui_page 'index.html'

client_script 'client.lua'
