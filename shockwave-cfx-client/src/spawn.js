import sample from 'lodash/sample';

const spawns = [{
  pos: [1082.25, -696.921, 58.0099],
  heading: 0.0
}, {
  pos: [389.6471, -356.0052, 48.02429],
  heading: -80.0
}, {
  pos: [-826.1856, -112.8301, 37.5022],
  heading: 25.0
}, {
  pos: [-764.2693, 49.75876, 50.17163],
  heading: -157.0
}];

export function RespawnPlayer(model) {
  const { pos, heading } = sample(spawns);
  const [x, y, z] = pos;

  return SpawnPlayer(model, x, y, z, heading);
}
