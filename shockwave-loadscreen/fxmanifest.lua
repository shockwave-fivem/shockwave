fx_version 'cerulean'
game 'gta5'

loadscreen_manual_shutdown 'yes'

file 'loadscreen.html'
file 'dist/loadscreen.js'
file 'public/cursor.png'

loadscreen 'loadscreen.html'

client_script 'loadscreen.lua'

export 'Shutdown'
