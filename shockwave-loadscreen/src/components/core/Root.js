import styled from 'styled-components';

const Root = styled.div`
  width: 100%;
  height: 100%;
  background-color: #2b2f35;
`;

export default Root;
