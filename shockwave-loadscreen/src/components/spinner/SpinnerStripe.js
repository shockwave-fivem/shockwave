import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';

// -----------------------------------------------------------------------------

const stretchDelay = ({ delay }) => delay;
const stretchAnimation = keyframes`
  0%, 40%, 100% { transform: scaleY(0.6); }
  20% { transform: scaleY(1.0); }
`;

// -----------------------------------------------------------------------------

const SpinnerStripe = styled.div`
  background-color: #7a7e82;
  height: 100%;
  width: 2px;
  display: inline-block;
  animation-name: ${stretchAnimation};
  animation-delay: ${stretchDelay};
  animation-duration: 1.2s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;

  :not(:last-child) {
    margin-right: 4px;
  }
`;

SpinnerStripe.propTypes = {
  delay: PropTypes.string.isRequired
};

// -----------------------------------------------------------------------------

export default SpinnerStripe;
