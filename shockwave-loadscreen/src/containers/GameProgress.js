import React from 'react';

import ProgressLabel from '../components/progress/ProgressLabel';
import ProgressBar from '../components/progress/ProgressBar';

// -----------------------------------------------------------------------------

const STAGES = [
  'INIT_CORE_1',
  'INIT_CORE_2',
  'INIT_CORE_3',
  'INIT_BEFORE_MAP_LOADED_1',
  'INIT_LOAD_MAP_1',
  'INIT_AFTER_MAP_LOADED_1',
  'INIT_AFTER_MAP_LOADED_2',
  'INIT_SESSION_1',
  'INIT_SESSION_2',
  'INIT_SESSION_3'
];

const calculateProgress = (stage, count, idx) => (
  (STAGES.indexOf(stage) / STAGES.length) +
  ((idx / count) / STAGES.length)
);

// -----------------------------------------------------------------------------

class GameProgress extends React.Component {
  constructor() {
    super();

    this.stage = '';
    this.count = 0;
    this.idx = 0;

    this.state = {
      message: 'Loading game...',
      progress: 0.0
    };

    window.addEventListener('message', (event) => {
      if (event.data.eventName === 'startInitFunctionOrder') {
        this.stage = `${event.data.type}_${event.data.order || 1}`;
        this.count = event.data.count;
        this.idx = 0;
      }

      if (event.data.eventName === 'startDataFileEntries') {
        this.stage = 'INIT_LOAD_MAP_1';
        this.count = event.data.count;
        this.idx = 0;
      }

      if (event.data.eventName === 'initFunctionInvoking') {
        const message = `${this.stage}: ${event.data.name}`;
        const progress = calculateProgress(this.stage, this.count, ++this.idx);

        this.setState({ message, progress });
      }

      if (event.data.eventName === 'performMapLoadFunction') {
        const progress = calculateProgress(this.stage, this.count, ++this.idx);

        this.setState({ progress });
      }

      if (event.data.eventName === 'onDataFileEntry') {
        this.setState({ message: `${this.stage}: ${event.data.name}` });
      }

      if (event.data.eventName === 'onLogLine') {
        this.setState({ message: event.data.message });
      }
    });
  }

  render() {
    return (
      <React.Fragment>
        <ProgressLabel message={this.state.message} />
        <ProgressBar progress={this.state.progress} />
      </React.Fragment>
    );
  }
}

// -----------------------------------------------------------------------------

export default GameProgress;
