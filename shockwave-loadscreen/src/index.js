import React from 'react';
import ReactDOM from 'react-dom';

import App from './containers/App';

const $root = document.querySelector('#root');
const $cursor = document.querySelector('#cursor');

window.addEventListener('mousemove', (event) => {
  $cursor.style.left = `${event.pageX}px`;
  $cursor.style.top = `${event.pageY}px`;
});

window.addEventListener('message', (event) => {
  if (event.data.shutdown) {
    $root.style.opacity = 0;
    $cursor.style.opacity = 0;
  }
});

ReactDOM.render(<App />, $root);
