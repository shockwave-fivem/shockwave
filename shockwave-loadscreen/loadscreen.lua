function Shutdown()
  if GetIsLoadingScreenActive() then
    SendLoadingScreenMessage(json.encode({ shutdown = true }))
    ShutdownLoadingScreen()

    SetTimeout(1000, function()
      ShutdownLoadingScreenNui()
    end)
  end
end

AddEventHandler('onClientPlayerSpawn', Shutdown)
