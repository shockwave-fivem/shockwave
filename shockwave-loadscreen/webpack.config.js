module.exports = {
  entry: './src/index.js',
  output: {
    filename: './loadscreen.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    }]
  }
};
